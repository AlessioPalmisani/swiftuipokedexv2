//
//  ContentView.swift
//  SwiftUIPokedexV2
//
//  Created by Alessio Palmisani on 25/01/2021.
//

import SwiftUI

struct ContentView: View {
    
    @State var pokemons: [PokeList] = []
    
    var service = PokeApiService()
    @State private var page = 0
    @State private var isLoading: Bool = false
    private let pageSize = 20
    
    var body: some View {
        
        NavigationView {
            List(pokemons) { pokemon in
                NavigationLink(destination: PokemonDetailView(name: pokemon.name, image: pokemon.image, pkmnNo: pokemon.pkmnNo)){
                    HStack{
                        Image(uiImage: pokemon.image)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 50, height: 50)
                        Text("# \(String(pokemon.pkmnNo))")
                        Text(pokemon.name)
                        
                        if !isLoading && pokemons.isLastItem(pokemon){
                            Divider()
                            ProgressView()
                        }
                        
                    }.onAppear {
                        listItemAppears(pokemon)
                    }
                }
            }.navigationBarTitle("Pokedex")
            .onAppear() {
                if pokemons.isEmpty {
                    service.getPokemonList(offset: 0){
                        self.pokemons = $0
                    }
                }
            }
        }
        
    }
}

private extension ContentView {
    
    func listItemAppears<PokeList: Identifiable>(_ pokemon: PokeList) {
        if !isLoading && self.pokemons.isLastItem(pokemon){
            isLoading = true
            page += 1
            getMoreItems(forPage: page, pageSize: pageSize)
            isLoading = false
        }
    }
    
}


private extension ContentView {
    
    
    func getMoreItems(forPage page: Int, pageSize: Int) {
        
        let max = pageSize * page
        service.getPokemonList(offset: max){
            self.pokemons = $0
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
