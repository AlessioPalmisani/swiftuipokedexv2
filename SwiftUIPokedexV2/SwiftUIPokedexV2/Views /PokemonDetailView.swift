//
//  PokemonDetailView.swift
//  SwiftUIPokedexV2
//
//  Created by Alessio Palmisani on 28/01/2021.
//

import SwiftUI

struct PokemonDetailView: View {
    
    var service = PokeApiService()
    @State var typesArray: [String] = []
    @State var details: Pokemon?
    var name = ""
    var image: UIImage?
    var pkmnNo: Int?
    
    var body: some View {
        ZStack(alignment: .topLeading){
            VStack{
                Spacer(minLength: 1)
                
                HStack(alignment: .center){
                    Image(uiImage: image ?? UIImage())
                        .resizable()
                        .scaledToFit()
                        .frame(width: 200, height: 200)
                    
                    VStack(alignment: .leading) {
                        Text("#\(pkmnNo ?? 0)")
                            .padding(.bottom)
                            .foregroundColor(.black)
                        
                        Text(name.capitalized)
                            .padding(.bottom)
                            .foregroundColor(.black)
                        
                        if typesArray.count == 1 {
                            Text("   \(typesArray.first ?? "Can't find type")   ")
                                .background(RoundedRectangle(cornerRadius: 4).foregroundColor(Color("\(typesArray.first ?? "normal")")))
                                .foregroundColor(.white)
                        } else if typesArray.count == 2 {
                            HStack {
                                Text("   \(typesArray.last ?? "Can't find type")   ")
                                    .background(RoundedRectangle(cornerRadius: 4).foregroundColor(Color("\(typesArray.last ?? "normal")")))
                                    .foregroundColor(.white)
                                Text("   \(typesArray.first ?? "Can't find type")   ")
                                    .background(RoundedRectangle(cornerRadius: 4).foregroundColor(Color("\(typesArray.first ?? "normal")")))
                                    .foregroundColor(.white)
                            }
                        }
                        
                    }
                    Spacer()
                }
                
                ZStack(alignment: .center) {
                    RoundedRectangle(cornerRadius: 25)
                        //                        .frame(width: screenWidth, height: (screenHeight * (3/4)))
                        .foregroundColor(.white)
                        .edgesIgnoringSafeArea(.bottom)
                    
                    VStack(alignment: .leading, spacing: 20) {
                        // STATS GRAPHS
                        Group {
                            Text("Base Stats")
                                .foregroundColor(Color("\(typesArray.last ?? "normal")"))
                                .padding(.top, 10)
                            
                            BarView(color: "\(typesArray.last ?? "normal")", stat: "HP", statValue: CGFloat(details?.stats[5].baseStat ?? 0), statValueText: details?.stats[5].baseStat ?? 0)
                            
                            BarView(color: "\(typesArray.last ?? "normal")", stat: "Attack", statValue: CGFloat(details?.stats[4].baseStat ?? 0), statValueText: details?.stats[4].baseStat ?? 0)
                            
                            BarView(color: "\(typesArray.last ?? "normal")", stat: "Defense", statValue: CGFloat(details?.stats[3].baseStat ?? 0), statValueText: details?.stats[3].baseStat ?? 0)
                            
                            BarView(color: "\(typesArray.last ?? "normal")", stat: "Sp. Atk", statValue: CGFloat(details?.stats[2].baseStat ?? 0), statValueText: details?.stats[2].baseStat ?? 0)
                            
                            BarView(color: "\(typesArray.last ?? "normal")", stat: "Sp. Def", statValue: CGFloat(details?.stats[1].baseStat ?? 0), statValueText: details?.stats[1].baseStat ?? 0)
                            
                            BarView(color: "\(typesArray.last ?? "normal")", stat: "Speed", statValue: CGFloat(details?.stats[0].baseStat ?? 0), statValueText: details?.stats[0].baseStat ?? 0)
                            
                        }
                        
                    }
                }
            }
        }.onAppear() {
            service.getPokemon(name: name) { details in
                if case .decoded(let details) = details{
                    self.details = details
                    self.appendDetailsData()
                }
            }
        }
    }
    
    func appendDetailsData() {
        for type in details?.types ?? [] {
            typesArray.append(type.type.name)
        }
    }
    struct PokemonDetailView_Previews: PreviewProvider {
        static var previews: some View {
            PokemonDetailView()
        }
    }
    
}
