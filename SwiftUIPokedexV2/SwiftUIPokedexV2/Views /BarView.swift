//
//  BarView.swift
//  SwiftUIPokedexV2
//
//  Created by Alessio Palmisani on 28/01/2021.
//

import SwiftUI

struct BarView: View {
    
    var color: String
    var stat: String
    var statValue: CGFloat
    var statValueText: Int
    let screenWidth = UIScreen.main.bounds.width
    
    var body: some View {
        HStack {
            Text(stat)
                .frame(width: 70, height: 20, alignment: .leading)
                .foregroundColor(.black)
            ZStack(alignment: .leading) {
                Capsule().frame(width: screenWidth * (5/8), height: 20)
                    .foregroundColor(.gray)
                Capsule().frame(width: statValue, height: 20)
                    .foregroundColor(Color(color))
                
            }
            Text("\(String(statValueText))")
                .foregroundColor(.black)
        }
    }
}
