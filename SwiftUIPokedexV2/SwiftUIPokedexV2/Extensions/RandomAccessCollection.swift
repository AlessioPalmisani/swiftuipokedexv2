//
//  RandomAccessCollection.swift
//  SwiftUIPokedexV2
//
//  Created by Alessio Palmisani on 28/01/2021.
//

import SwiftUI

extension RandomAccessCollection where Self.Element: Identifiable {
    func isLastItem<PokemonList: Identifiable>(_ item: PokemonList) -> Bool {
        guard !isEmpty else {
            return false
        }
        
        guard let itemIndex = firstIndex(where: { $0.id.hashValue == item.id.hashValue }) else {
            return false
        }
        
        let distance = self.distance(from: itemIndex, to: endIndex)
        return distance == 1
    }
}
