//
//  PokeApiService.swift
//  SwiftUIPokedexV2
//
//  Created by Giuseppe Mazzilli on 26/01/2021.
//

import Foundation
import SwiftUI

class PokeApiService: RestClient {
    
    enum APIService: REstService {
        case getResourceList(Int)
        case getPokemon(String)
        case getSprites(String)
        
        var url: URL {
            switch self {
            case .getResourceList(let offset): return URL(string: "https://pokeapi.co/api/v2/pokemon?limit=20&offset=\(String(offset))")!
            case .getPokemon(let name): return URL(string: "https://pokeapi.co/api/v2/pokemon/\(name)")!
            case .getSprites(let urlString): return URL(string: urlString)!
            }
        }

    }
    
    var delegate: URLSessionDelegate?
    private var pokeList: [PokeList] = []
    private var nameApiResults: [NamedAPIResource] = []
    
    func getResourceList(offset: Int ,completion: @escaping (REstAPIResult<ResourceList>) -> Void) {
        fetch(APIService.getResourceList(offset), completion: completion)
    }
    func getPokemon(name: String, completion: @escaping (REstAPIResult<Pokemon>) -> Void) {
        fetch(APIService.getPokemon(name), completion: completion)
    }
    func getSprites(url: String, completion: @escaping (REstAPIResult<Data>) -> Void) {
        fetch(APIService.getSprites(url), completion: completion)
    }
    
    
    func getPokemonList(offset: Int, completion: @escaping ([PokeList]) -> Void){
        
        getResourceList(offset: offset) { result in
            if case .decoded(let result) = result {
                for (index , pokemon) in result.results.enumerated() {
                    self.getPokemon(name: pokemon.name) { details in
                        if case .decoded(let details) = details {
                            self.getSprites(url: details.sprites.frontDefault) { sprite in
                                if case .encoded(let data) = sprite {
                                   guard let image = UIImage(data: data) else { return }
                                   
                                    self.pokeList.append(PokeList(pkmnNo: details.id,
                                                                  name: details.name,
                                                                  image: image))
                                    
                                    self.pokeList.sort{
                                        $0.pkmnNo < $1.pkmnNo
                                    }
                                    
                                    if index == (result.results.count - 1) {
                                        completion(self.pokeList)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    }
    
    

