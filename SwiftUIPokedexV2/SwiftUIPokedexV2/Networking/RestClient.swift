//
//  RestClient.swift
//  SwiftUIPokedexV2
//
//  Created by Giuseppe Mazzilli on 26/01/2021.
//

import Foundation

enum REstAPIResult<T> {
    case decoded(T)
    case encoded(Data)
    case failure
}

protocol RestClient: APIClient {
    
    func fetch<T: Decodable>(_ api: REstService, completion: @escaping (REstAPIResult<T>) -> Void)
    
}

extension RestClient {
    
    func fetch<T: Decodable>(_ api: REstService, completion: @escaping (REstAPIResult<T>) -> Void) {
        
        let request = URLRequest(url: api.url,
                                 cachePolicy: api.cachePolicy,
                                 timeoutInterval: api.timeInterval)
        
        fetch(request) { result in
            switch result {
            case .success(let data):
                do {
                    try completion(.decoded(JSONDecoder().decode(T.self, from: data)))
                    print("REst service returned decodable data")
                } catch {
                    print("REst service returned encoded data")
                    completion(.encoded(data))
                }
            default:
                print("REst service failed to retrieve data")
                completion(.failure)
            }
        }
        
    }
    
    
}
