//
//  NetworkManager.swift
//  SwiftUIPokedexV2
//
//  Created by Alessio Palmisani on 25/01/2021.
//

import SwiftUI

enum APIResult {
    case success(Data)
    case failure(Data?, URLResponse)
    case error(Error)
}

protocol APIClient {
    
    var delegate: URLSessionDelegate? { get }
    var configuration: URLSessionConfiguration { get }
    
    func debugResponse(_ response: URLResponse)
    func fetch(_ request: URLRequest, completion: @escaping (APIResult) -> Void)
}

extension APIClient {
    
    var configuration: URLSessionConfiguration { .default }
    
    func debugResponse(_ response: URLResponse) {
        guard let response = response as? HTTPURLResponse else { return }
        print("Server return code = \(response.statusCode), ", HTTPURLResponse.localizedString(forStatusCode: response.statusCode))
    }

    func fetch(_ request: URLRequest, completion: @escaping (APIResult) -> Void) {
        
        let session = URLSession(configuration: configuration,
                                 delegate: delegate,
                                 delegateQueue: .main)
        
        session.dataTask(with: request) { (data, response, error) in
            switch (data, response, error) {
            // Error case
            case (_, _, let .some(error)):
                print("Client error: ", error.localizedDescription)
                completion(.error(error))
            // Success case
            case (let .some(data), _, _) where data.count > 0:
                print("Client retrieved data successfully")
                print(String(decoding: data, as: UTF8.self))
                completion(.success(data))
            // Failure case
            case (_, let .some(response), _):
                debugResponse(response)
                completion(.failure(data, response))
            default:
                print("Unhandled urlsession datatask response")
            }
        }.resume()
        
    }

}

