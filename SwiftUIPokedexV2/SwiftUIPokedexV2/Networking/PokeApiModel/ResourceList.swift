//
//  ResourceList.swift
//  SwiftUIPokedexV2
//
//  Created by Giuseppe Mazzilli on 26/01/2021.
//

import Foundation

struct ResourceList: Decodable {
    let count: Int
    let next: String
    let previous: String?
    let results: [NamedAPIResource]
}
