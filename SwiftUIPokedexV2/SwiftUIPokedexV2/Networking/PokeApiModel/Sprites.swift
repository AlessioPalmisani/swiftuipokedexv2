//
//  Sprites.swift
//  SwiftUIPokedexV2
//
//  Created by Alessio Palmisani on 27/01/2021.
//

import Foundation

struct Sprites: Decodable {
    
    let frontDefault: String
    let backDefault: String
    
    enum CodingKeys: String, CodingKey {
        case frontDefault = "front_default"
        case backDefault = "back_default"
    }
    
}
