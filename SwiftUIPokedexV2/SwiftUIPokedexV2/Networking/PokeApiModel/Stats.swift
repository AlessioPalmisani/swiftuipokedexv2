//
//  Stats.swift
//  SwiftUIPokedexV2
//
//  Created by Alessio Palmisani on 27/01/2021.
//

import Foundation

struct Stats: Decodable {
    let stat: NamedAPIResource
    let baseStat: Int
    
    enum CodingKeys: String, CodingKey {
        case stat
        case baseStat = "base_stat"
    }
}
