//
//  Pokemon.swift
//  SwiftUIPokedexV2
//
//  Created by Alessio Palmisani on 27/01/2021.
//

import Foundation

struct Pokemon: Decodable {
    
    let id: Int
    let name: String
    let sprites: Sprites
    let types: [Types]
    let stats: [Stats]
    
}
