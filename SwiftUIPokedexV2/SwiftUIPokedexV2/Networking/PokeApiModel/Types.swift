//
//  Types.swift
//  SwiftUIPokedexV2
//
//  Created by Alessio Palmisani on 27/01/2021.
//

import Foundation

struct Types: Decodable {
    let slot: Int
    let type: NamedAPIResource
}
