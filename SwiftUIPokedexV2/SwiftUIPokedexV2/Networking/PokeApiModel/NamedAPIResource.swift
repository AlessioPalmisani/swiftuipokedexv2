//
//  NamedAPIResource.swift
//  SwiftUIPokedexV2
//
//  Created by Giuseppe Mazzilli on 26/01/2021.
//

import Foundation

struct NamedAPIResource: Decodable, Identifiable {
    let id = UUID()
    let name: String
    let url: String
    
    enum CodingKeys: String, CodingKey {
        case name, url
    }
}
