//
//  PokeList.swift
//  SwiftUIPokedexV2
//
//  Created by Alessio Palmisani on 27/01/2021.
//

import Foundation
import SwiftUI

struct PokeList: Identifiable {
    
    let id = UUID()
    let pkmnNo: Int
    let name: String
    let image: UIImage

}
