//
//  RestService.swift
//  SwiftUIPokedexV2
//
//  Created by Giuseppe Mazzilli on 26/01/2021.
//

import Foundation

protocol REstService {
    var url: URL { get }
    var cachePolicy: URLRequest.CachePolicy { get }
    var timeInterval: TimeInterval { get }
}

extension REstService {
    var cachePolicy: URLRequest.CachePolicy { .reloadIgnoringLocalCacheData }
    var timeInterval: TimeInterval { 30 }
}
